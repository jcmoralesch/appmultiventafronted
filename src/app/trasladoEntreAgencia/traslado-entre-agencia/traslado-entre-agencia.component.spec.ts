import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrasladoEntreAgenciaComponent } from './traslado-entre-agencia.component';

describe('TrasladoEntreAgenciaComponent', () => {
  let component: TrasladoEntreAgenciaComponent;
  let fixture: ComponentFixture<TrasladoEntreAgenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrasladoEntreAgenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrasladoEntreAgenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
