import { Component, OnInit, Input } from '@angular/core';
import { TrasladoEntreAgenciaService } from 'src/app/model/service/traslado-entre-agencia.service';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { AgenciaService } from 'src/app/model/service/agencia.service';
import { Agencia } from 'src/app/model/class/agencia';
import { TrasladoEntreAgencia } from 'src/app/model/class/traslado-entre-agencia';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-traslado',
  templateUrl: './form-traslado.component.html',
  styleUrls: ['./form-traslado.component.css']
})
export class FormTrasladoComponent implements OnInit {

  @Input() cantidadProducto:CantidadProducto;
  public agencias:Agencia[];
  public trasladoEntreAgencia:TrasladoEntreAgencia = new TrasladoEntreAgencia();

  constructor(public trasladoEntreAgenciaService:TrasladoEntreAgenciaService,
              private agenciaService:AgenciaService) { }

  ngOnInit() {
    this.getAllAgencias();
  }
  

  public cerrarModal():void{
    this.trasladoEntreAgenciaService.cerrarModal();
  }

  private getAllAgencias():void{
    //this.agenciaService.getAllInCantidadProducto().subscribe(
      this.agenciaService.getAll().subscribe(
      agencias=> this.agencias=agencias
    )
  }

  public storeTraslado():void{
    this.trasladoEntreAgencia.agenciaOrigen=this.cantidadProducto.agencia;
    this.trasladoEntreAgencia.cantidadProducto=this.cantidadProducto;

    this.trasladoEntreAgenciaService.store(this.trasladoEntreAgencia).subscribe(
      traslado=>{
          Swal.fire('Exito',`Se traslado correctamente el producto ${traslado.cantidadProducto.producto.nombre} 
                     a la agencia ${traslado.agenciaDestino.nombre}`,'success');
          this.cerrarModal();
          this.trasladoEntreAgenciaService.notificarCambio.emit();
      },
      error=>{
        Swal.fire('ERROR', `No se pudo hacer el traslado, El problema se podria deber porque el 
                   producto que esta intentando trasladar, no esta registrado en la agencia ${this.trasladoEntreAgencia.agenciaDestino.nombre}`,'error');
      }
    )
   
  }

}
