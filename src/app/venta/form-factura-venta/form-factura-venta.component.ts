import { Component, OnInit } from '@angular/core';
import { Factura } from 'src/app/model/class/factura';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { FacturaService } from 'src/app/model/service/factura.service';
import { ItemFactura } from 'src/app/model/class/item-factura';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { ClienteService } from 'src/app/model/service/cliente.service';
import { Cliente } from 'src/app/model/class/cliente';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { LoginService } from '../../model/service/login.service';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-form-factura-venta',
  templateUrl: './form-factura-venta.component.html',
  styleUrls: ['./form-factura-venta.component.css']
})
export class FormFacturaVentaComponent implements OnInit {
  currencyPipe= new CurrencyPipe('es-GT')
  titulo: string = 'Nueva Factura';
  factura: Factura = new Factura();
  autocompleteControl = new FormControl();
  autocompleteControlCliente = new FormControl();
  productosFiltrados: Observable<CantidadProducto[]>;
  clientesFiltrados:Observable<Cliente[]>;
  public cliente:Cliente;
  public clienteNew:Cliente=new Cliente();

  constructor(private facturaService: FacturaService,
              private toastr:ToastrService,
              private clienteService:ClienteService,
              public loginService:LoginService) { }

  ngOnInit() {
      this.cargarProductos();
      this.clientesFiltrados=this.autocompleteControlCliente.valueChanges.pipe(
        map(value => typeof value === 'string' ? value: value.nombre),
        flatMap(value => value ? this._filterCliente(value): [])
      );
      this.actualizarTablaDeseModal();

  }

  private cargarProductos():void{
    this.productosFiltrados = this.autocompleteControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.producto.nombre), 
        flatMap(value => value ? this._filter(value) : [])
      );
  }

  private _filter(value: string): Observable<CantidadProducto[]> {
    const filterValue = value.toUpperCase();
    return this.facturaService.filtrarProductos(filterValue);
  }

  private _filterCliente(value:string):Observable<Cliente[]>{
    const filterValue = value.toUpperCase();
    return this.clienteService.filtrarCliente(filterValue);
  }

  mostrarNombre(producto?: CantidadProducto): string | undefined {
    return producto ? producto.producto.nombre : undefined;
  }

  mostrarNombreCliente(cliente?:Cliente): string | undefined{
    return cliente ? cliente.nombre : undefined;
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
  
    let producto = event.option.value as CantidadProducto;
    if (this.existeItem(producto.id)) {
      this.incrementaCantidad(producto.id);
    } else {
      let nuevoItem = new ItemFactura();
      nuevoItem.producto = producto;
      this.factura.items.push(nuevoItem);
    }

    
    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();

  }

  seleccionarCliente(event: MatAutocompleteSelectedEvent): void {
  
    let cliente = event.option.value as Cliente;
    this.clienteNew=cliente;
    this.autocompleteControlCliente.setValue('');
    event.option.focus();
    event.option.deselect();

  }

  actualizarCantidad(id: number, event: any,cantBase:number): void {
    let cantidad: number = event.target.value as number;

    if (cantidad>cantBase) {
      Swal.fire("Error",`Ya no hay productos en EXISTENCIA, solo se puede vender ${cantBase}`,'error');
      return ;
    }

    if (cantidad == 0) {
      return this.eliminarItemFactura(id);
    }

    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.producto.producto.id) {
        item.cantidad = cantidad;
      }
      return item;
    });
  }

  actualizarPrecio(id: number, event: any,pBase:number): void {
    let precio: number = event.target.value as number;

    if (precio<pBase) {
      Swal.fire('Error', 'Esta ingresando un precio menor a lo establecido, Si no lo corrige no podra registrar la venta', 'error');
      return ;
    }

    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.producto.producto.id) {
        item.producto.producto.precioVenta= precio;
        item.precioVenta=item.producto.producto.precioVenta;
      }
      return item;
    });
  }

  existeItem(id: number): boolean {
    let existe = false;
    this.factura.items.forEach((item: ItemFactura) => {
      if (id === item.producto.id) {
        existe = true;
      }
    });
    return existe;
  }

  incrementaCantidad(id: number): void {
    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.producto.id) {
        ++item.cantidad;
      }
      return item;
    });
  }

  eliminarItemFactura(id: number): void {
    this.factura.items = this.factura.items.filter((item: ItemFactura) => id !== item.producto.producto.id);
  }

  create(facturaForm,num:number): void {

    if (this.factura.items.length == 0) {
      this.autocompleteControl.setErrors({ 'invalid': true });
    }  
    if (facturaForm.form.valid && this.factura.items.length > 0) {
      if(this.clienteNew.nombre=="" && this.clienteNew.id==0){
        this.factura.cliente = null;
      }
      else{
          this.factura.cliente = this.clienteNew;
      }
     
      this.facturaService.create(this.factura).subscribe(factura => {
        this.facturaService.notificarCambio.emit();//para actualizar datos en la tabla
        this.toastr.success('Venta registrada con exito', 'Exito');
        let i=0;
        let cantidad=this.factura.items.length;
        for(i;i<=cantidad;i++){
          this.factura.items.pop();
        }
        if(num==1){
          this.generarpdf(factura);
        }
        this.clienteNew.nombre="";
        this.clienteNew.id=0;
        this.cargarProductos();
      },
      err=>{
          Swal.fire('Error','No se pudo registrar la venta','error');
      });
    }
    
  }

  private actualizarTablaDeseModal():void{
    this.clienteService.notificarCambio.subscribe(
      cliente=>this.clienteNew=cliente
    )
  }


  //Generar comprobante de pago en PDF

  generarpdf(fac:Factura){
    var img = new Image();
     img.src = 'assets/img/conexion2.png';
    var doc = new jsPDF();
    doc.addImage(img,'png',70,10,50,30)
    doc.setFillColor(51, 76, 255);
    doc.rect(0, 0, 220, 6, 'F'); // TOP filled square blue
    doc.setFontSize(12);
    doc.text(`${fac.usuario.personal.agencia.nombre}`, 10, 12);
    doc.setFontSize(10);
    doc.text(`${fac.usuario.personal.agencia.ubicacion}`, 10, 18);
    doc.text('FACTURA', 155, 12);
    doc.text('Fecha', 160, 18);
    doc.line(140,19,190,19);
    doc.text(`${fac.fechaRegistro} ${fac.horaRegistro}`,150,24);
    doc.text('No. de comprobante', 150, 30);
    doc.line(140,31,190,31);
    doc.text(`${fac.id}`,160,36);
    doc.text('CLIENTE',10,42);
    doc.line(10,43,80,43)
    if(fac.cliente==null){
      doc.text('Consumidor final',10,48);
    }else{
      doc.text(`${fac.cliente.nombre} ${fac.cliente.apellido}`,10,48);
      doc.text(`${fac.cliente.direccion}`,10,54);
      doc.text(`${fac.cliente.telefono}`,10,60);
    }
    doc.text('VENDEDOR',120,42);
    doc.line(120,43,190,43)
    doc.text(fac.usuario.personal.nombre +' ' + fac.usuario.personal.apellido,120,48);
    
    doc.autoTable({
         head: this.headRows(),
         if(){},
         body: this.bodyRows(fac),
         startY: 66,
         showHead: 'firstPage',
         theme: 'striped',
         styles: {
          lineColor: [44, 62, 80],
          lineWidth: 0.3,
        },
     });
     doc.text(`TOTAL: ${this.currencyPipe.transform(this.factura.total, 'GTQ')}`, 155, doc.autoTable.previous.finalY + 8);
     doc.line(155,(doc.autoTable.previous.finalY + 9),195,(doc.autoTable.previous.finalY + 9));
     doc.setFillColor(51, 76, 255);
     doc.rect(0, (doc.autoTable.previous.finalY + 35), 220, 6, 'F'); // BOTTOM filled square blue
     doc.save('Factura-N'+fac.id+'.pdf');
     //doc.autoPrint('Factura-No-'+fac.id+'.pdf');
  }
  
  
  headRows() {
    return [{producto: 'Descripción',precio: 'Precio', cantidad: 'Unidades', total:'Sub-total'}];
  }
  
  bodyRows(fac:Factura) {
    let body = [];
    let contador:number=0;
    let t= fac.items.length;
    let preciov:number=0;
    

    for (var j = 1; j <= t; j++) {
      if(fac.items[contador].precioVenta==0){
            preciov=fac.items[contador].producto.producto.precioVenta; 
      }
      else{
        preciov=fac.items[contador].precioVenta;
      }
        body.push({
            no: j,
            producto: fac.items[contador].producto.producto.nombre,
            precio: this.currencyPipe.transform(preciov,'GTQ'),
            cantidad:fac.items[contador].cantidad,
            total:this.currencyPipe.transform(fac.items[contador].importe, 'GTQ')
        });
        contador++;
    }  
    return body;
  }

  public abrirModalCliente():void{
    this.clienteService.abrirModal();
  }

}

