import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFacturaVentaComponent } from './form-factura-venta.component';

describe('FormFacturaVentaComponent', () => {
  let component: FormFacturaVentaComponent;
  let fixture: ComponentFixture<FormFacturaVentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFacturaVentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFacturaVentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
