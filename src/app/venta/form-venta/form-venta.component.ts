import { Component, OnInit, Input } from '@angular/core';
import { VentaService } from 'src/app/model/service/venta.service';
import { Venta } from 'src/app/model/class/venta';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';

@Component({
  selector: 'app-form-venta',
  templateUrl: './form-venta.component.html',
  styleUrls: ['./form-venta.component.css']
})
export class FormVentaComponent implements OnInit {

  @Input() cantidadProducto:CantidadProducto;
  public venta:Venta = new Venta();

  constructor(public ventaService:VentaService,
              private toastr:ToastrService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.ventaService.cerrarModal();
  }

  public store():void{
    if(this.venta.precioVenta<this.cantidadProducto.producto.precio){
      Swal.fire('Error','Esta ingresando un precio muy bajo a lo autorizado','warning');
      return;
    }
    this.venta.producto=this.cantidadProducto.producto;
    this.ventaService.store(this.venta).subscribe(
      venta=>{
        this.cantidadProducto.cantidad=this.cantidadProducto.cantidad-1;
        this.ventaService.notificarCambio.emit(this.cantidadProducto);//para actualizar datos en la tabla
        this.cerrarModal();
        this.toastr.success('Venta registrada con éxito','Success');
        this.venta.precioVenta=0.00;
        this.venta.producto=null;
      },
      err=>{
         this.toastr.error('Error, operacion fallida','ERROR');
      }
    )

  }

}
