import { Component, OnInit, Input } from '@angular/core';
import { VentaService } from 'src/app/model/service/venta.service';
import { Venta } from 'src/app/model/class/venta';
import { LoginService } from 'src/app/model/service/login.service';
import { URL_BACKEND_IMAGE } from 'src/app/config/config';

@Component({
  selector: 'app-detalle-venta',
  templateUrl: './detalle-venta.component.html',
  styleUrls: ['./detalle-venta.component.css']
})
export class DetalleVentaComponent implements OnInit {
  @Input() venta:Venta;
  public urlImage:string=URL_BACKEND_IMAGE;
  
  constructor(public ventaService:VentaService,
              public loginService:LoginService) { }
  

  ngOnInit() {
  }

  public cerrarModal():void{
    this.ventaService.cerrarModal();
  }

}
