import { Component, OnInit, ViewChild } from '@angular/core';
import { VentaService } from 'src/app/model/service/venta.service';
import { MatSort, MatPaginator,MatTableDataSource } from '@angular/material';
import { Venta } from 'src/app/model/class/venta';
import { ConsultaFecha } from 'src/app/model/class/consulta-fecha';
import { AgenciaService } from 'src/app/model/service/agencia.service';
import { Agencia } from 'src/app/model/class/agencia';
import { LoginService } from 'src/app/model/service/login.service';

@Component({
  selector: 'app-list-venta',
  templateUrl: './list-venta.component.html',
  styleUrls: ['./list-venta.component.css']
})
export class ListVentaComponent implements OnInit {

  public dataSource:any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public consultaFecha:ConsultaFecha = new ConsultaFecha();
  public agencias:Agencia[];
  public total:number;
  public ganancia:number;
  public venta:Venta;
  public valor:boolean=true;
  public agencia:string='';
  
  constructor(private ventaService:VentaService,
              private agenciaService:AgenciaService,
              public loginService:LoginService) { }

  ngOnInit() {
    this.getAllAgencia();
    this.getAllAgenciaAndDate();
    
  }

  displayedColumns = ['Codigo','Nombre','Precio','Ganancia','Fecha','Hora','Acciones'];

  private getAllAgencia():void{
    this.agenciaService.getAll().subscribe(
      agencias=>this.agencias=agencias
    )
  }

  private getAllAgenciaAndDate():void{
    this.ventaService.getAllAgenciaAndDate().subscribe(
      response=>{
        this.total=response.map(t=> t.precioVenta).reduce((acc,value)=>acc+value,0);
        this.ganancia=response.map(t=> t.ganancia).reduce((acc,value)=>acc+value,0);
        this.valor=false;
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  public getByFechaAndAgencia():void{
     this.ventaService.getByFechaAndAgencia(this.consultaFecha.fecha1.toISOString(),
                                           this.consultaFecha.fecha2.toISOString(),this.consultaFecha.agencia).subscribe(
      response=>{
        this.total=response.map(t=> t.precioVenta).reduce((acc,value)=>acc+value,0);
        this.ganancia=response.map(t=> t.ganancia).reduce((acc,value)=>acc+value,0);
        this.valor=false;
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
     )
  }

  private tableFilter(): (data: Venta, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
     return data.producto.codigo.indexOf(filter) != -1;
    }
    return filterFunction;
  }

  public applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
   // filterValue = filterValue.toUpperCase();
    this.dataSource.filter=filterValue;
  }

  public modalVenta(row:any):void{
    this.venta=row;
    this.ventaService.abrirModal();
  }

}
