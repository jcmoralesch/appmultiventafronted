import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/model/service/login.service';

@Component({
  selector: 'app-nav-producto',
  templateUrl: './nav-producto.component.html',
  styleUrls: ['./nav-producto.component.css']
})
export class NavProductoComponent implements OnInit {

  constructor(public loginService:LoginService) { }

  ngOnInit() {
  }

}
