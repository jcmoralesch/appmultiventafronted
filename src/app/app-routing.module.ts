import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgenciaComponent } from './agencia/agencia.component';
import { FormAgenciaComponent } from './agencia/form-agencia/form-agencia.component';
import { PersonalComponent } from './personal/personal.component';
import { FormPersonalComponent } from './personal/form-personal/form-personal.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { FormProveedorComponent } from './proveedores/form-proveedor/form-proveedor.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { MarcaComponent } from './marca/marca.component';
import { ProductoComponent } from './producto/producto.component';
import { FormProductoComponent } from './producto/form-producto/form-producto.component';
import { DetalleComponent } from './producto/detalle/detalle.component';
import { ImagenProductoComponent } from './producto/imagen-producto/imagen-producto.component';
import { LoginComponent } from './usuario/login/login.component';
import { IngresoComponent } from './ingreso-producto/ingreso/ingreso.component';
import { ListTrasladoComponent } from './traslado/list-traslado/list-traslado.component';
import { CantidadProductoComponent } from './producto/cantidad-producto/cantidad-producto.component';
import { TodoCantidadProductoComponent } from './producto/todo-cantidad-producto/todo-cantidad-producto.component';
import { ListVentaComponent } from './venta/list-venta/list-venta.component';
import { ListUsuarioComponent } from './usuario/list-usuario/list-usuario.component';
import { EditUsuarioComponent } from './usuario/edit-usuario/edit-usuario.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ModificarUsuarioComponent } from './usuario/modificar-usuario/modificar-usuario.component';
import { FormFacturaVentaComponent } from './venta/form-factura-venta/form-factura-venta.component';
import { ListFacturaComponent } from './factura/list-factura/list-factura.component';

const routes: Routes = [
  {path:'',redirectTo:'/login',pathMatch:'full'},
  {path: 'login',component:LoginComponent},
  {path:'agencia',component:AgenciaComponent},
  {path:'agencia/form',component:FormAgenciaComponent},
  {path:'personal',component:PersonalComponent},
  {path:'personal/page/:page',component:PersonalComponent},
  {path:'personal/form',component:FormPersonalComponent},
  {path:'personal/form/:id',component:FormPersonalComponent},
  {path:'proveedor',component:ProveedoresComponent},
  {path:'proveedor/form',component:FormProveedorComponent},
  {path:'proveedor/page/:page',component:ProveedoresComponent},
  {path:'proveedor/form/:id',component:FormProveedorComponent},
  {path:'categoria',component:CategoriaComponent},
  {path:'marca',component:MarcaComponent},
  {path:'list-facturas',component:ListFacturaComponent},
  {path:'producto',component:ProductoComponent},
  {path:'producto/form',component:FormProductoComponent},
  {path:'producto/form/:id',component:FormProductoComponent},
  {path:'producto/detalle/:id',component:DetalleComponent},
  {path:'producto/images',component:ImagenProductoComponent},
  {path:'ingreso',component:IngresoComponent},
  {path:'traslado',component:ListTrasladoComponent},
  {path:'cantidad',component:CantidadProductoComponent},
  {path:'todo-cantidad',component:TodoCantidadProductoComponent},
  {path:'venta',component:ListVentaComponent},
  {path:'venta-factura',component:FormFacturaVentaComponent},
  {path:'list-usuario',component:ListUsuarioComponent},
  {path:'usuario/edit/:id',component:EditUsuarioComponent},
  {path:'usuario/form/:id',component:UsuarioComponent},
  {path:'usuario/update-user/:username',component:ModificarUsuarioComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
