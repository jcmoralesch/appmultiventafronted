import { Component, OnInit } from '@angular/core';
import { ProveedorService } from '../model/service/proveedor.service';
import { Proveedor } from '../model/class/proveedor';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})
export class ProveedoresComponent implements OnInit {

  public proveedors:Proveedor[];
  public paginador:any;
  public title:string='';

  constructor(private proveedorService:ProveedorService,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getAll();
  }

  private getAll():void{
    this.activatedRoute.paramMap.subscribe(
      param=>{
        let page:number =+param.get('page');
        if(!page){
          page=0;
        }
        this.proveedorService.getAll(page).subscribe(
          response=>{
            this.proveedors=response.content as Proveedor[];
            this.paginador=response;
          }
        )
      }
    )
  }

  public delete(proveedor:any):void{
    Swal.fire({
        title: 'Esta seguro?',
        text: `De eliminar proveedor  ${proveedor.nombre} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Eliminar !'
      }).then((result) => {

        if (result.value) {
         this.proveedorService.delete(proveedor).subscribe(
           response=>{
             this.proveedors=this.proveedors.filter(prov=> prov !==proveedor);
             Swal.fire(
               'Eliminado!',
               `El proveedor ${proveedor.nombre} ha sido eliminado`,
               'success'
             )
           }
         )

        }
      })
  }


}
