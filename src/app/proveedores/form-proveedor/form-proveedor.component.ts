import { Component, OnInit } from '@angular/core';
import { Proveedor } from 'src/app/model/class/proveedor';
import { Empresa } from 'src/app/model/class/empresa';
import { ProveedorService } from 'src/app/model/service/proveedor.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-proveedor',
  templateUrl: './form-proveedor.component.html',
  styleUrls: ['./form-proveedor.component.css']
})
export class FormProveedorComponent implements OnInit {

  public proveedor:Proveedor = new Proveedor();
  public empresa:Empresa = new Empresa();
  public errores:string[];

  constructor(private proveedorService:ProveedorService,
              private router:Router,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.cargarProveedor();
  }
  
  public store():void{
    this.proveedor.empresa=this.empresa;
    this.proveedorService.store(this.proveedor).subscribe(
      proveedor=>{
          this.router.navigate(['/proveedor']);
          Swal.fire('Registro nuevo',`Provedor ${proveedor.nombre} registrado con exito`,'success');
      },
      err=>{
        this.errores=err.error.errors as string[];
      }
    )
  }

  private cargarProveedor():void{
     this.activatedRoute.params.subscribe(
         params=>{
           let id=params['id'];
           if(id){
             this.proveedorService.getById(id).subscribe(
               proveedor=>{
                 this.proveedor=proveedor;
                 this.empresa=proveedor.empresa;
                }
             )
           }
         }
     )
  }
  
  public update():void{
    this.proveedor.empresa=this.empresa;
    this.proveedorService.update(this.proveedor).subscribe(
      proveedor=>{
        this.router.navigate(['/proveedor']);
        Swal.fire('Datos Actualizados',`Datos actualizados correctamente`,'success');
      },
      err=>{
        
      }
    )
  }


}
