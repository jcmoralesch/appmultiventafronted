import { Component, OnInit, Input } from '@angular/core';
import { Marca } from 'src/app/model/class/marca';
import { MarcaService } from 'src/app/model/service/marca.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-form-marca',
  templateUrl: './form-marca.component.html',
  styleUrls: ['./form-marca.component.css']
})
export class FormMarcaComponent implements OnInit {

  @Input() marca:Marca;

  constructor(public marcaService:MarcaService,private toastr:ToastrService) { }

  ngOnInit() {
  }

  public store():void{

    this.marcaService.store(this.marca).subscribe(
      marca=>{
        this.marcaService.notificarCambio.emit();//para actualizar datos en la tabla
        this.cerrarModal();
        this.toastr.success('Operación realizada con éxito','Success');
      },
      err=>{
      }
    )}

  public cerrarModal():void{
    this.marcaService.cerrarModal();
  }

}
