import { Component, OnInit } from '@angular/core';
import { MarcaService } from '../model/service/marca.service';
import { Marca } from '../model/class/marca';

@Component({
  selector: 'app-marca',
  templateUrl: './marca.component.html',
  styleUrls: ['./marca.component.css']
})
export class MarcaComponent implements OnInit {

  public marcas:Marca[];
  public marca:Marca = new Marca();

  constructor(public marcaService:MarcaService) { }

  ngOnInit() {
    this.getAll();
    this.actualizarTablaDeseModal();
  }

  public getAll():void{
      this.marcaService.getAll().subscribe(
        marca=>this.marcas=marca
      )
  }

  public abrirModal():void{
    this.marcaService.abrirModal();
  }

  private actualizarTablaDeseModal():void{
    this.marcaService.notificarCambio.subscribe(
      marca=>this.getAll()
    )
  }

}
