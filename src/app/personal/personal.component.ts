import { Component, OnInit } from '@angular/core';
import { PersonalService } from '../model/service/personal.service';
import {Personal} from '../model/class/personal';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.css']
})
export class PersonalComponent implements OnInit {

  public personals:Personal[];
  paginador:any;

  constructor(private personalService:PersonalService,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getAll(); 
  }

  private getAll():void{

    this.activatedRoute.paramMap.subscribe(
      params=>{
        let page:number=+params.get('page');
        if(!page){
          page=0;
        }
        this.personalService.getAll(page).subscribe(   
          response=>{
            this.personals=response.content as Personal[];
            this.paginador=response;
          }
        )
      }) 
  }

  delete(personal:any):void{
    Swal.fire({
        title: 'Esta seguro?',
        text: `De eliminar usuario al  ${personal.nombre} !`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, Eliminar !'
      }).then((result) => {

        if (result.value) {
         this.personalService.delete(personal.id).subscribe(
           response=>{
             this.personals=this.personals.filter(usr=> usr !==personal);
             Swal.fire(
               'Eliminado!',
               `El usuario ${personal.nombre} ha sido eliminado`,
               'success'
             )
           }
         )

        }
      })
  }

}
