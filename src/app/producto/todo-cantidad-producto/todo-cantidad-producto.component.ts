import { Component, OnInit, ViewChild } from '@angular/core';
import { CantidadProductoService } from 'src/app/model/service/cantidad-producto.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { ProductoService } from 'src/app/model/service/producto.service';
import { Producto } from 'src/app/model/class/producto';
import { TrasladoEntreAgenciaService } from 'src/app/model/service/traslado-entre-agencia.service';
import { LoginService } from 'src/app/model/service/login.service';

@Component({
  selector: 'app-todo-cantidad-producto',
  templateUrl: './todo-cantidad-producto.component.html',
  styleUrls: ['./todo-cantidad-producto.component.css']
})
export class TodoCantidadProductoComponent implements OnInit {

  public dataSource:any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public producto:Producto;
  public cantidadProducto:CantidadProducto;

  constructor(private cantidadProductoService:CantidadProductoService,
              public productoService:ProductoService,
              public trasladoEntreAgenciaService:TrasladoEntreAgenciaService,
              public loginService:LoginService) { }

  ngOnInit() {
    this.getAll();
    this.actualizarTablaDeseModal();
  }

  displayedColumns = ['Codigo','Nombre', 'Precio', 'Categoria', 'Marca','Agencia', 'Disponible', 'Acciones'];
  private getAll():void{
    this.cantidadProductoService.getAllAgencias().subscribe(
      response => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      }
    )
  }

  private tableFilter(): (data: CantidadProducto, filter: string) => boolean {
  
    let filterFunction = function (data, filter): boolean {
      return data.producto.codigo.indexOf(filter) != -1
        || data.producto.nombre.indexOf(filter) != -1
        || data.producto.categoria.categoria.indexOf(filter) != -1
        || data.producto.marca.marca.indexOf(filter) != -1
        || data.agencia.nombre.indexOf(filter) !=-1
    }
    return filterFunction;
  }

  public applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter = filterValue;
  }


  public findyByCantidad(value:any):void{
    this.cantidadProductoService.getByCantidadAll(value).subscribe(
      response => {
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = response;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.dataSource.filterPredicate = this.tableFilter();
      }
    )
    
  }

  private actualizarTablaDeseModal():void{
    this.trasladoEntreAgenciaService.notificarCambio.subscribe(
      cantidad=>this.getAll()
    )
  }

  public abrirModal(row:any):void{
    this.producto=row.producto;
    this.productoService.abrirModal();
  }

  public abrirModalTraslado(row):void{
    this.cantidadProducto=row;
    this.trasladoEntreAgenciaService.abrirModal();
  }

}
