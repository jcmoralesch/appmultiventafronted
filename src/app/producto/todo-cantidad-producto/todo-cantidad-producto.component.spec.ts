import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoCantidadProductoComponent } from './todo-cantidad-producto.component';

describe('TodoCantidadProductoComponent', () => {
  let component: TodoCantidadProductoComponent;
  let fixture: ComponentFixture<TodoCantidadProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoCantidadProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoCantidadProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
