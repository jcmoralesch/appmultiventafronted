import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/model/class/producto';
import { ProductoService } from 'src/app/model/service/producto.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { Categoria } from 'src/app/model/class/categoria';
import { CategoriaService } from 'src/app/model/service/categoria.service';
import { MarcaService } from 'src/app/model/service/marca.service';
import { ProveedorService } from 'src/app/model/service/proveedor.service';
import { Marca } from 'src/app/model/class/marca';
import { Proveedor } from 'src/app/model/class/proveedor';

@Component({
  selector: 'app-form-producto',
  templateUrl: './form-producto.component.html',
  styleUrls: ['./form-producto.component.css']
})
export class FormProductoComponent implements OnInit {

  public producto:Producto=new Producto();
  public categorias:Categoria[];
  public marcas:Marca[];
  public proveedors:Proveedor[];
  public marca:Marca = new Marca();
  public categoria:Categoria= new Categoria();

  constructor(private productoService:ProductoService,
              private router:Router,
              private categoriaService:CategoriaService,
              private marcaService:MarcaService,
              private proveedorService:ProveedorService,
              private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.getAllCategoria();
    this.getAllMarca();
    this.getAllProveedor();
    this.cargarProducto();
    this.actualizarTablaDeseModal();
    this.actualizarTablaDeseModalCategoria();
  }

  public store():void{
    this.productoService.store(this.producto).subscribe(
      producto=>{
        this.router.navigate(['/producto']);
        Swal.fire('Registro nuevo','producto registrado correctamente','success');
      }
    )
  }

  public cargarProducto():void{
     this.activatedRoute.params.subscribe(
       param=>{
         let id=param['id'];
         if(id){
           this.productoService.getById(id).subscribe(
             producto=>this.producto=producto
           )
         }
       }
     )
  }

  public update():void{
    this.productoService.update(this.producto).subscribe(
      producto=>{
        this.router.navigate(['/producto']);
        Swal.fire('Actualizado','Datos de producto actualizado correctamente','success');
      }
    )
  }

  private getAllCategoria():void{
    this.categoriaService.findAll().subscribe(
      categoria=>this.categorias=categoria
    )
  }

  private getAllMarca():void{
    this.marcaService.getAll().subscribe(
      marca=>this.marcas=marca
    )
  }

  private getAllProveedor():void{
    this.proveedorService.getAllPersonal().subscribe(
      proveedor=>this.proveedors=proveedor
    )
  }

  public abrirModal():void{
    this.marcaService.abrirModal();
  }

  public abrirModalCategoria():void{
    this.categoriaService.abrirModal();
  }

  public cerrarModal():void{
    this.marcaService.cerrarModal();
  }

  private actualizarTablaDeseModal():void{
    this.marcaService.notificarCambio.subscribe(
      marca=>this.getAllMarca()
    )
  }

  private actualizarTablaDeseModalCategoria():void{
    this.categoriaService.notificarCambio.subscribe(
      categoria=>this.getAllCategoria()
    )
  }

  compararCategoria(o1:Categoria,o2:Categoria):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararMarca(o1:Marca,o2:Marca):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

  compararProveedor(o1:Proveedor,o2:Proveedor):boolean{
    if(o1===undefined && o2===undefined){
      return true;
    }
    return o1===null || o2===null || o1===undefined || o2===undefined? false: o1.id===o2.id;
  }

}
