import { Component, OnInit,ViewChild } from '@angular/core';
import { ProductoService } from '../model/service/producto.service';
import {MatTableDataSource,MatSort,MatPaginator} from '@angular/material';
import { Producto } from '../model/class/producto';
import { UsuarioService } from '../model/service/usuario.service';
import { LoginService } from '../model/service/login.service';
import { IngresoProductoService } from '../model/service/ingreso-producto.service';


@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  constructor(private productoService:ProductoService,
              private usuarioService:UsuarioService,
              public loginService:LoginService,
              private ingresoProductoService:IngresoProductoService) { }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource:any;
  producto:Producto;

  ngOnInit() {
    this.getAll();
  }

  displayedColumns = ['Codigo','Nombre','Precio','Categoria','Marca','Acciones'];

  private getAll():void{
    this.productoService.getAll().subscribe(
      response=>{
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  tableFilter(): (data: Producto, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
      return data.codigo.indexOf(filter) != -1
      || data.nombre.indexOf(filter) !=-1
      || data.marca.marca.indexOf(filter) !=-1
      || data.categoria.categoria.indexOf(filter) !=-1;
            
    }
    return filterFunction;

 }

 applyFilter(filterValue: any) {
  filterValue = filterValue.trim();
  filterValue = filterValue.toUpperCase();
  this.dataSource.filter=filterValue;
}

public abrirModal(row):void{

  this.producto=row;
  this.productoService.abrirModal();
}

public ingresoProducto(row):void{
  this.producto=row;
  this.ingresoProductoService.abrirModal();

}

}
