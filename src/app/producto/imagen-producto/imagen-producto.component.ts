import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/model/service/producto.service';
import { ProductoConsulta } from 'src/app/model/class/producto-consulta';
import { Marca } from 'src/app/model/class/marca';
import { MarcaService } from 'src/app/model/service/marca.service';
import { CategoriaService } from 'src/app/model/service/categoria.service';
import { Categoria } from 'src/app/model/class/categoria';
import { LoginService } from 'src/app/model/service/login.service';
import { UsuarioService } from 'src/app/model/service/usuario.service';
import { CantidadProductoService } from 'src/app/model/service/cantidad-producto.service';
import { CantidadProducto } from 'src/app/model/class/cantidad-producto';
import { URL_BACKEND_IMAGE } from 'src/app/config/config';

@Component({
  selector: 'app-imagen-producto',
  templateUrl: './imagen-producto.component.html',
  styleUrls: ['./imagen-producto.component.css']
})
export class ImagenProductoComponent implements OnInit {

  productos:CantidadProducto[];
  marcas:Marca[];
  productoConsulta:ProductoConsulta= new ProductoConsulta();
  categorias:Categoria[];
  agencia:string="";
  public urlImage:string=URL_BACKEND_IMAGE;

  constructor(private productoService:ProductoService,
              private marcaService:MarcaService,
              private categoriaService:CategoriaService,
              private loginService:LoginService,
              private usuarioService:UsuarioService,
              private cantidadProductoService:CantidadProductoService) { }

  ngOnInit() {
    this.findAgenciaNombre();
    this.getMarcas();
    this.getCategorias();
  }

  findAgenciaNombre():void{
    this.usuarioService.findByUbicacion(this.loginService.id).subscribe(
      agencia=>{
        this.getAll(agencia.ubicacion);
        this.agencia=agencia.ubicacion;
      }
    )
  }

  getAll(agencia:string):void{
    this.cantidadProductoService.getByAgenciaAndCantidad(agencia).subscribe(
      productos=>this.productos=productos
    )
  }

  getMarcas():void{
    this.marcaService.getAll().subscribe(
      marcas=>this.marcas=marcas
    )
  }

  getCategorias():void{
    this.categoriaService.findAll().subscribe(
      categorias=>this.categorias=categorias
    )
  }


  buscarProducto():void{
    this.cantidadProductoService.findProductosByFilter(this.productoConsulta.marca,this.agencia,
                                        this.productoConsulta.categoria).subscribe(
      productos=>this.productos=productos
    );
  }
  

}
