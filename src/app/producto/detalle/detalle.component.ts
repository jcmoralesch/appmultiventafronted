import { Component, OnInit, Input } from '@angular/core';
import { ProductoService } from 'src/app/model/service/producto.service';
import { Producto } from 'src/app/model/class/producto';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { LoginService } from 'src/app/model/service/login.service';
import { URL_BACKEND_IMAGE } from 'src/app/config/config';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  @Input() producto:Producto;
  private imagenSeleccionada:File;
  public progreso:number=0;
  public urlImage:string=URL_BACKEND_IMAGE;
  

  constructor(public productoService:ProductoService,
              public loginService:LoginService) { }

  ngOnInit() {
    
  }

  seleccionaFoto(event){
    this.imagenSeleccionada=event.target.files[0];
    this.progreso=0;
    if(this.imagenSeleccionada.type.indexOf('image')<0){
      Swal.fire('Error:','El archivo no es una imagen','error');
      this.imagenSeleccionada=null;
    }
    if(this.imagenSeleccionada.name.length>22){
      Swal.fire('Error','El nombre de la imagen es demasiado largo','warning')
    }
  }

  subirFoto():void{

    if(!this.imagenSeleccionada){
      Swal.fire('Error:','Debe seleccionar una imagen','error');
    }else{
    this.productoService.subirFoto(this.imagenSeleccionada,this.producto.id).subscribe(
      event=>{
        if(event.type ===HttpEventType.UploadProgress){
          this.progreso= Math.round((event.loaded/event.total)*100);
        }else if(event.type === HttpEventType.Response){
          let response:any = event.body;
          this.producto=response.producto as Producto;

          Swal.fire('La foto se ha subido con exito',response.mensaje,'success');
         
        }
        
      }
    )
    }
  }

  public cerrarModal():void{
    this.productoService.cerrarModal();
  }

}
