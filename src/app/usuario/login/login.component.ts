import { Component, OnInit } from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import { Usuario } from 'src/app/model/class/usuario';
import { LoginService } from 'src/app/model/service/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title:string='Iniciar session!';
  usuario:Usuario;

  constructor(private toastr:ToastrService,
              public loginService:LoginService,
              private router:Router) {
     this.usuario= new Usuario();
   }

  ngOnInit() {

    if(this.loginService.isAuthenticated()){
      this.toastr.info("Ya ha iniciado session", 'Information');
      this.router.navigate(['/cantidad']);
    }
  }

  login():void{

    if(this.usuario.username==null || this.usuario.password==null){
      this.errorsmsg();
      return;
    }

    this.loginService.login(this.usuario).subscribe(
      response=>{
      //  let objetoPayload=JSON.parse(atob(response.access_token.split(".")[1]));
        this.loginService.guardarUsuario(response.access_token);
        this.loginService.guardarToken(response.access_token);
        let usuario=this.loginService.usuario;
        this.router.navigate(['/cantidad']);
        this.toastr.success('Bienvenido nuevamente',`${usuario.username}`);

      },
      err=>{
        if(err.status==400){
          this.toastr.error("Usuario o Password Incorrecto",'Error')
        }
      }
    )
  }

  errorsmsg(){ 
    this.toastr.error("Usuario o Password vacios",'Error')
  }
}
