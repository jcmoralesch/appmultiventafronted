import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/class/usuario';
import { UsuarioService } from 'src/app/model/service/usuario.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/model/service/login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modificar-usuario',
  templateUrl: './modificar-usuario.component.html',
  styleUrls: ['./modificar-usuario.component.css']
})
export class ModificarUsuarioComponent implements OnInit {

  usuario: Usuario = new Usuario();

  constructor(private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loginService: LoginService) { }

  ngOnInit() {
    this.findUsuario();
  }

  private findUsuario():void{
    this.activatedRoute.params.subscribe(
      params=>{
        let username=params['username'];
        if(username){
          this.usuarioService.findByUsername(username).subscribe(
            usuario=>this.usuario=usuario
          )
        }
      }
    )
  }

  public update(){
    this.usuarioService.updatePassUser(this.usuario).subscribe(
      usuario=>{
        this.loginService.logout();
        this.router.navigate(['/login']);
        Swal.fire('Datos actualizados','Los datos fueron actualizados exitosamente','success');
      }
    )
  }

}
