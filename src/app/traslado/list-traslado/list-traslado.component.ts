import { Component, OnInit, ViewChild } from '@angular/core';
import { TrasladoProductoService } from 'src/app/model/service/traslado-producto.service';
import { MatTableDataSource ,MatSort,MatPaginator} from '@angular/material';
import { TrasladoProducto } from 'src/app/model/class/traslado-producto';
import { ConsultaFecha } from 'src/app/model/class/consulta-fecha';
import { LoginService } from '../../model/service/login.service';

@Component({
  selector: 'app-list-traslado',
  templateUrl: './list-traslado.component.html',
  styleUrls: ['./list-traslado.component.css']
})
export class ListTrasladoComponent implements OnInit {

  public dataSource:any;
  public consultaFecha:ConsultaFecha = new ConsultaFecha();
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public trasladoProducto:TrasladoProducto;
  public valor:boolean=true;


  constructor(private trasladoProductoService:TrasladoProductoService,
              public loginService:LoginService) { }

  ngOnInit() {
  }

  displayedColumns = ['Codigo','Nombre','Cantidad','Agencia','Fecha','Hora','Acciones'];

  private tableFilter(): (data: TrasladoProducto, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
     return data.ingresoProducto.producto.codigo.indexOf(filter) != -1
         || data.ingresoProducto.producto.nombre.indexOf(filter) != -1
         || data.agencia.nombre.indexOf(filter) != -1;
    }
    return filterFunction;
  }

  public applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    //filterValue = filterValue.toUpperCase();
    this.dataSource.filter=filterValue;
  }

  public modalTraslado(row:any):void{
     this.trasladoProducto=row;
     this.trasladoProductoService.abrirModal();
  }
  
  public getByDate():void{
     this.trasladoProductoService.getByDate(this.consultaFecha.fecha1.toISOString(),
                                  this.consultaFecha.fecha2.toISOString()).subscribe(
      response=>{
        this.valor=false;
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
     )
  }
}
