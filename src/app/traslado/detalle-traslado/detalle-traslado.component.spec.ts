import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleTrasladoComponent } from './detalle-traslado.component';

describe('DetalleTrasladoComponent', () => {
  let component: DetalleTrasladoComponent;
  let fixture: ComponentFixture<DetalleTrasladoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleTrasladoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleTrasladoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
