import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/model/service/cliente.service';
import { Cliente } from 'src/app/model/class/cliente';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-cliente',
  templateUrl: './form-cliente.component.html',
  styleUrls: ['./form-cliente.component.css']
})
export class FormClienteComponent implements OnInit {

  public cliente:Cliente = new Cliente();

  constructor(public clienteService:ClienteService,
              private toastr:ToastrService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.clienteService.cerrarModal();
  }

  public store():void{
    this.clienteService.store(this.cliente).subscribe(
      cliente=>{
        this.clienteService.notificarCambio.emit(cliente);//para actualizar datos en la tabla
        this.cerrarModal();
        this.toastr.success('Cliente registado','Success');
      },
      err=>{
        Swal.fire('Error','No se pudo registrar al cliente', 'error');
      }
    )
  }

}
