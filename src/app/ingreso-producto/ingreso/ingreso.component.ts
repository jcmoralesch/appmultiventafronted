import { Component, OnInit,ViewChild} from '@angular/core';
import { IngresoProductoService } from 'src/app/model/service/ingreso-producto.service';
import { IngresoProducto } from 'src/app/model/class/ingreso-producto';
import { MatTableDataSource ,MatSort,MatPaginator} from '@angular/material';
import { TrasladoProductoService } from 'src/app/model/service/traslado-producto.service';
import { ConsultaFecha } from 'src/app/model/class/consulta-fecha';
import { LoginService } from '../../model/service/login.service';


@Component({
  selector: 'app-ingreso',
  templateUrl: './ingreso.component.html',
  styleUrls: ['./ingreso.component.css']
})
export class IngresoComponent implements OnInit {

  public ingresoProducto:IngresoProducto;
  public dataSource:any;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  consultaFecha:ConsultaFecha = new ConsultaFecha();
  public total:number;
  

  constructor(private ingresoProductoService:IngresoProductoService,
              private trasladoProductoService:TrasladoProductoService,
              public loginService:LoginService) { }

  ngOnInit() {

    this.getAll();
    this.actualizarTablaDeseModal();
  }

  displayedColumns = ['Codigo','Nombre','Cantidad','Precio','Total','Fecha','Hora','Traslado','Acciones'];

  private getAll():void{
    
    this.ingresoProductoService.getAll().subscribe(
      response=>{
        this.total=response.map(t=> t.total).reduce((acc,value)=>acc+value,0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }

    )
  }

  private actualizarTablaDeseModal():void{
    this.trasladoProductoService.notificarCambio.subscribe(ingresoProducto=>{      
      this.dataSource.data= this.dataSource.data.map(ingresoProductoOriginal=>{
        if(ingresoProducto.id == ingresoProductoOriginal.id){
          ingresoProductoOriginal.traslado = ingresoProducto.traslado;
        }
        return ingresoProductoOriginal;
      })     
    })
  }

  tableFilter(): (data: IngresoProducto, filter: string) => boolean {
    let filterFunction = function(data, filter): boolean {
     return data.producto.codigo.indexOf(filter) != -1
     || data.producto.nombre.indexOf(filter) != -1;
    }
    return filterFunction;
  }

  applyFilter(filterValue: any) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toUpperCase();
    this.dataSource.filter=filterValue;
  }

  modalTraslado(row):void{
    this.ingresoProducto=row;
    this.trasladoProductoService.abrirModal();
  }

  public getByDate():void{
    this.ingresoProductoService.findByFechaIngresoBetween(this.consultaFecha.fecha1.toISOString(),
                                                  this.consultaFecha.fecha2.toISOString()).subscribe(
      response=>{
        this.total=response.map(t=> t.total).reduce((acc,value)=>acc+value,0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=response;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
        this.dataSource.filterPredicate=this.tableFilter();
      }
    )
  }

  public modalDetalle(row:any):void{
    this.ingresoProducto=row;
    this.ingresoProductoService.abrirModal();
  }

}
