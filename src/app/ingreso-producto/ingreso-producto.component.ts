import { Component, OnInit, Input } from '@angular/core';
import { Producto } from '../model/class/producto';
import { IngresoProductoService } from '../model/service/ingreso-producto.service';
import { IngresoProducto } from '../model/class/ingreso-producto';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingreso-producto',
  templateUrl: './ingreso-producto.component.html',
  styleUrls: ['./ingreso-producto.component.css']
})
export class IngresoProductoComponent implements OnInit {

  @Input() producto:Producto;
  public ingresoProducto:IngresoProducto = new IngresoProducto();

  constructor(public ingresoProductoService:IngresoProductoService,
             private toastrService:ToastrService,
             private router:Router) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.ingresoProductoService.cerrarModal();
  }

  public store(){
    this.ingresoProducto.producto=this.producto;
    this.ingresoProductoService.store(this.ingresoProducto).subscribe(
      ingresoProducto=>{
        this.toastrService.success('Operación realizada con éxito','Exito');
        this.cerrarModal();
        this.ingresoProducto.cantidad=0;
        this.router.navigate(['/producto']);
      },
      error=>{
        this.toastrService.error('ERROR', 'No se pudo realizar la operacion');
        this.cerrarModal();
      }
    )
  }

}
