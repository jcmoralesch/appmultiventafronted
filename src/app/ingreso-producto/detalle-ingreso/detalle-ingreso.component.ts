import { Component, OnInit, Input } from '@angular/core';
import { IngresoProductoService } from 'src/app/model/service/ingreso-producto.service';
import { IngresoProducto } from 'src/app/model/class/ingreso-producto';
import { URL_BACKEND_IMAGE } from 'src/app/config/config';

@Component({
  selector: 'app-detalle-ingreso',
  templateUrl: './detalle-ingreso.component.html',
  styleUrls: ['./detalle-ingreso.component.css']
})
export class DetalleIngresoComponent implements OnInit {

  @Input() ingresoProducto:IngresoProducto;
  public urlImage:string=URL_BACKEND_IMAGE;
  constructor(public ingresoProductoService:IngresoProductoService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
     this.ingresoProductoService.cerrarModal();
  }

}
