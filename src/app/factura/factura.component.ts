import { Component, OnInit, ViewChild } from '@angular/core';
import { FacturaService } from '../model/service/factura.service';
import { Factura } from '../model/class/factura';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html',
  styleUrls: ['./factura.component.css']
})
export class FacturaComponent implements OnInit {

  public facturas:Factura[];
  public factura:Factura;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource:any;
  public total:number;
  

  constructor(private facturaService:FacturaService) { }

  ngOnInit() {
    this.getAll();
    this.actualizarTablaDeseModal(); 
  }

  displayedColumns = ['Fecha','Hora','Cliente','Total','Acciones'];

  private getAll():void{
    this.facturaService.getAll().subscribe(
      facturas=>{
        this.total=facturas.map(t=> t.total).reduce((acc,value)=>acc+value,0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data=facturas;
        this.dataSource.sort=this.sort;
        this.dataSource.paginator=this.paginator;
      }
    )
  }
  
  private actualizarTablaDeseModal():void{
    this.facturaService.notificarCambio.subscribe(
      factura=>this.getAll()
    )
  }

  public abrirModal(row:Factura):void{
    this.factura=row;
    this.facturaService.abrirModal();
  }

  
}
