import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FacturaService } from '../../model/service/factura.service';
import { ConsultaFecha } from '../../model/class/consulta-fecha';
import { AgenciaService } from '../../model/service/agencia.service';
import { Agencia } from '../../model/class/agencia';
import { Factura } from '../../model/class/factura';
import { ItemFactura } from 'src/app/model/class/item-factura';

@Component({
  selector: 'app-list-factura',
  templateUrl: './list-factura.component.html',
  styleUrls: ['./list-factura.component.css']
})
export class ListFacturaComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource: any;
  public total: number;
  public consultaFecha: ConsultaFecha = new ConsultaFecha();
  public agencias: Agencia[];
  public factura: Factura;
  public listItemFacturas: ItemFactura[];
  public listItemFacturaEnviar: ItemFactura[];

  constructor(public facturaService: FacturaService, private agenciaService: AgenciaService) { }

  ngOnInit() {
    this.getByCurrentDate();
    this.getAllAgencias();
  }

  displayedColumns = ['Id', 'Fecha', 'Hora', 'Cliente', 'Agencia', 'Usuario', 'Total', 'Acciones'];

  private getByCurrentDate(): void {
    this.facturaService.getByCurrenDate().subscribe(
      facturas => {
        this.listItemFacturas = [];
        facturas.map(response => {
          let num = response.id;
          response.items.map(resp => {
            resp.numeroFacutura = num;
            this.listItemFacturas.push(resp)
          })
        });

        this.total = facturas.map(t => t.total).reduce((acc, value) => acc + value, 0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = facturas;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    )
  }

  public getByDate() {
    this.facturaService.finByFechaBetweenOrAgencia(this.consultaFecha.fecha1.toISOString(), this.consultaFecha.fecha2.toISOString(), this.consultaFecha.agencia).subscribe(

      facturas => {
        this.listItemFacturas = [];
        facturas.map(response => {
          let num = response.id;
          response.items.map(resp => {
            resp.numeroFacutura = num;
            this.listItemFacturas.push(resp)
          })
        });
        this.total = facturas.map(t => t.total).reduce((acc, value) => acc + value, 0);
        this.dataSource = new MatTableDataSource();
        this.dataSource.data = facturas;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      })
  }

  private getAllAgencias(): void {
    this.agenciaService.getAll().subscribe(
      response => this.agencias = response
    )
  }

  public abrirModalDetalle(row) {
    this.factura = row;
    this.facturaService.abrirModal();
  }

  public mostrarDetalleVenta() {
    this.listItemFacturaEnviar = this.listItemFacturas
    this.facturaService.abrirModalItem();
  }

}
