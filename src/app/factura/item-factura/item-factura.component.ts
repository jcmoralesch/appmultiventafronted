import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FacturaService } from '../../model/service/factura.service';
import { ItemFactura } from '../../model/class/item-factura';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-item-factura',
  templateUrl: './item-factura.component.html',
  styleUrls: ['./item-factura.component.css']
})
export class ItemFacturaComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataSource: any;

  @Input() listItemFactura: ItemFactura[];

  constructor(public facturaService: FacturaService) { }

  ngOnInit() {
    this.mostrarDetalleFactura();

  }

  displayedColumns = ['Id', 'Cantidad', 'PrecioVenta', 'Importe', 'Agencia','Codigo','Categoria','Nombre','PrecioCompra'];

  public mostrarDetalleFactura() {

    this.dataSource = new MatTableDataSource();
    this.dataSource.data = this.listItemFactura;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
