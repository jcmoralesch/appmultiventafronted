import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemFacturaComponent } from './item-factura.component';

describe('ItemFacturaComponent', () => {
  let component: ItemFacturaComponent;
  let fixture: ComponentFixture<ItemFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemFacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
