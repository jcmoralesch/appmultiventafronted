import { Component, OnInit, Input } from '@angular/core';
import { FacturaService } from 'src/app/model/service/factura.service';
import { Factura } from 'src/app/model/class/factura';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { CurrencyPipe } from '@angular/common';


@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.css']
})
export class DetalleFacturaComponent implements OnInit {

  @Input() factura:Factura;
  currencyPipe= new CurrencyPipe('es-GT')

  constructor(public facturaService:FacturaService) { }

  ngOnInit() {
  }

  public cerrarModal():void{
    this.facturaService.cerrarModal();
  }

 
  generarpdf(fac:Factura){
    var img = new Image();
     img.src = 'assets/img/conexion2.png';
    var doc = new jsPDF();
    doc.addImage(img,'png',70,10,50,30)
    doc.setFillColor(51, 76, 255);
    doc.rect(0, 0, 220, 6, 'F'); // TOP filled square blue
    doc.setFontSize(12);
    doc.text(`${fac.usuario.personal.agencia.nombre}`, 10, 12);
    doc.setFontSize(10);
    doc.text(`${fac.usuario.personal.agencia.ubicacion}`, 10, 18);
    doc.text('FACTURA', 155, 12);
    doc.text('Fecha', 160, 18);
    doc.line(140,19,190,19);
    doc.text(`${fac.fechaRegistro} ${fac.horaRegistro}`,150,24);
    doc.text('No. de comprobante', 150, 30);
    doc.line(140,31,190,31);
    doc.text(`${fac.id}`,160,36);
    doc.text('CLIENTE',10,42);
    doc.line(10,43,80,43)
    if(fac.cliente==null){
      doc.text('Consumidor final',10,48);
    }else{
      doc.text(`${fac.cliente.nombre} ${fac.cliente.apellido}`,10,48);
      doc.text(`${fac.cliente.direccion}`,10,54);
      doc.text(`${fac.cliente.telefono}`,10,60);
    }
    doc.text('VENDEDOR',120,42);
    doc.line(120,43,190,43)
    doc.text(fac.usuario.personal.nombre +' ' + fac.usuario.personal.apellido,120,48);
    
    doc.autoTable({
         head: this.headRows(),
         if(){},
         body: this.bodyRows(fac),
         startY: 66,
         showHead: 'firstPage',
         theme: 'striped',
         styles: {
          lineColor: [44, 62, 80],
          lineWidth: 0.3,
        },
     });
     doc.text(`TOTAL: ${this.currencyPipe.transform(this.factura.total, 'GTQ')}`, 155, doc.autoTable.previous.finalY + 8);
     doc.line(155,(doc.autoTable.previous.finalY + 9),195,(doc.autoTable.previous.finalY + 9));
     doc.setFillColor(51, 76, 255);
     doc.rect(0, (doc.autoTable.previous.finalY + 35), 220, 6, 'F'); // BOTTOM filled square blue
     doc.save('Factura-N'+fac.id+'.pdf');
     //doc.autoPrint('Factura-No-'+fac.id+'.pdf');
  }
  
  headRows() {
    return [{producto: 'Descripción',precio: 'Precio', cantidad: 'Unidades', total:'Sub-total'}];
  }
  
   bodyRows(fac:Factura) {
    let body = [];
    let contador:number=0;
    let t= fac.items.length;
    let preciov:number=0;
    

    for (var j = 1; j <= t; j++) {
      if(fac.items[contador].precioVenta==0){
            preciov=fac.items[contador].producto.producto.precioVenta; 
      }
      else{
        preciov=fac.items[contador].precioVenta;
      }
        body.push({
            no: j,
            producto: fac.items[contador].producto.producto.nombre,
            precio: this.currencyPipe.transform(preciov,'GTQ'),
            cantidad:fac.items[contador].cantidad,
            total:this.currencyPipe.transform(fac.items[contador].importe, 'GTQ')
        });
        contador++;
    }  
    return body;
  }

}
