import { BrowserModule } from '@angular/platform-browser';
import {MatFormFieldModule,MatInputModule,MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule,LOCALE_ID } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AgenciaComponent } from './agencia/agencia.component';
import { FormAgenciaComponent } from './agencia/form-agencia/form-agencia.component';
import {FormsModule} from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { PersonalComponent } from './personal/personal.component';
import { FormPersonalComponent } from './personal/form-personal/form-personal.component';
import { PersonalPaginatorComponent } from './paginator/personal-paginator/personal-paginator.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { NavLateralComponent } from './nav-lateral/nav-lateral.component';
import { FormProveedorComponent } from './proveedores/form-proveedor/form-proveedor.component';
import { ProveedorPaginatorComponent } from './paginator/proveedor-paginator/proveedor-paginator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CategoriaComponent } from './categoria/categoria.component';
import { MarcaComponent } from './marca/marca.component';
import { ProductoComponent } from './producto/producto.component';
import { FormProductoComponent } from './producto/form-producto/form-producto.component'; 
import {MatTableModule,MatPaginatorModule,MatSortModule,MatButtonToggleModule} from '@angular/material/';
import { DetalleComponent } from './producto/detalle/detalle.component'; 
import { registerLocaleData } from '@angular/common';
import localeESGTQ from '@angular/common/locales/es-GT';
import { ImagenProductoComponent } from './producto/imagen-producto/imagen-producto.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { LoginComponent } from './usuario/login/login.component';
import { AuthInterceptor } from './usuario/interceptors/auth.interceptor';
import { TokenInterceptor } from './usuario/interceptors/token.interceptor';
import { IngresoProductoComponent } from './ingreso-producto/ingreso-producto.component';
import { IngresoComponent } from './ingreso-producto/ingreso/ingreso.component';
import { TrasladoComponent } from './traslado/traslado.component';
import { ListTrasladoComponent } from './traslado/list-traslado/list-traslado.component';
import { CantidadProductoComponent } from './producto/cantidad-producto/cantidad-producto.component';
import { NavProductoComponent } from './nav-lateral/nav-producto/nav-producto.component';
import { DetalleIngresoComponent } from './ingreso-producto/detalle-ingreso/detalle-ingreso.component';
import { DetalleTrasladoComponent } from './traslado/detalle-traslado/detalle-traslado.component';
import { FormVentaComponent } from './venta/form-venta/form-venta.component';
import { TodoCantidadProductoComponent } from './producto/todo-cantidad-producto/todo-cantidad-producto.component';
import { ListVentaComponent } from './venta/list-venta/list-venta.component';
import { DetalleVentaComponent } from './venta/detalle-venta/detalle-venta.component';
import { ListUsuarioComponent } from './usuario/list-usuario/list-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditUsuarioComponent } from './usuario/edit-usuario/edit-usuario.component';
import { ModificarUsuarioComponent } from './usuario/modificar-usuario/modificar-usuario.component';
import { FormFacturaVentaComponent } from './venta/form-factura-venta/form-factura-venta.component';
import { FacturaComponent } from './factura/factura.component';
import { FormMarcaComponent } from './marca/form-marca/form-marca.component';
import { FormCategoriaComponent } from './categoria/form-categoria/form-categoria.component';
import { FormClienteComponent } from './cliente/form-cliente/form-cliente.component';
import { DetalleFacturaComponent } from './factura/detalle-factura/detalle-factura.component';
import { TrasladoEntreAgenciaComponent } from './trasladoEntreAgencia/traslado-entre-agencia/traslado-entre-agencia.component';
import { FormTrasladoComponent } from './trasladoEntreAgencia/form-traslado/form-traslado.component';
import { MatTableExporterModule } from 'mat-table-exporter';
import { ListFacturaComponent } from './factura/list-factura/list-factura.component';
import { ItemFacturaComponent } from './factura/item-factura/item-factura.component';
registerLocaleData(localeESGTQ, 'es-GT');


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AgenciaComponent,
    FormAgenciaComponent,
    PersonalComponent,
    FormPersonalComponent,
    PersonalPaginatorComponent,
    ProveedoresComponent,
    NavLateralComponent,
    FormProveedorComponent,
    ProveedorPaginatorComponent,
    CategoriaComponent,
    MarcaComponent,
    ProductoComponent,
    FormProductoComponent,
    DetalleComponent,
    ImagenProductoComponent,
    UsuarioComponent,
    LoginComponent,
    IngresoProductoComponent,
    IngresoComponent,
    TrasladoComponent,
    ListTrasladoComponent,
    CantidadProductoComponent,
    NavProductoComponent,
    DetalleIngresoComponent,
    DetalleTrasladoComponent,
    FormVentaComponent,
    TodoCantidadProductoComponent,
    ListVentaComponent,
    DetalleVentaComponent,
    ListUsuarioComponent,
    EditUsuarioComponent,
    ModificarUsuarioComponent,
    FormFacturaVentaComponent,
    FacturaComponent,
    FormMarcaComponent,
    FormCategoriaComponent,
    FormClienteComponent,
    DetalleFacturaComponent,
    TrasladoEntreAgenciaComponent,
    FormTrasladoComponent,
    ListFacturaComponent,
    ItemFacturaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(
      {
        //positionClass:'top-left',
        closeButton:true
      }
    ),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonToggleModule,
    MatFormFieldModule, 
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatTableExporterModule

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-GT' },
    {provide:HTTP_INTERCEPTORS,useClass: TokenInterceptor, multi:true},
    {provide:HTTP_INTERCEPTORS,useClass: AuthInterceptor, multi:true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
