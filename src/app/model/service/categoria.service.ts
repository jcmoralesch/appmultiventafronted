import { Injectable, EventEmitter } from '@angular/core';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { Categoria } from '../class/categoria';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private urlEndPoint:string=URL_BACKEND+'/categoria';
  public modal:boolean =false;
  private _notificarCambio= new EventEmitter<any>();
  
  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  public store(categoria:Categoria):Observable<Categoria>{
    return this.http.post<Categoria>(`${this.urlEndPoint}`,categoria,{headers:HTTPHEADERS}).pipe(
      map((response:any)=>response.categoria as Categoria),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public findAll():Observable<Categoria[]>{
    return this.http.get<Categoria[]>(`${this.urlEndPoint}`);
  }

  public abrirModal(){
    this.modal=true;
  }

  public cerrarModal(){
    this.modal=false;
  }
}
