import { Injectable, EventEmitter } from '@angular/core';
import {URL_BACKEND} from '../../config/config';
import { HttpClient } from '@angular/common/http';
import { TrasladoProducto } from '../class/traslado-producto';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TrasladoProductoService {

  private urlEndPoint:string=URL_BACKEND+'/traslado-producto';
  public modal:Boolean=false;
  private _notificarCambio= new EventEmitter<any>();

  constructor(private http:HttpClient) { }

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  public store(trasladoProducto:TrasladoProducto):Observable<TrasladoProducto>{
    return this.http.post<TrasladoProducto>(`${this.urlEndPoint}`,trasladoProducto).pipe(
      map((response:any)=>response.trasladoProducto as TrasladoProducto),
      catchError(e=>{
        if(e.status==400){
          return throwError(e)
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getByDate(fecha1:string,fecha2:string):Observable<TrasladoProducto[]>{
    return this.http.get<TrasladoProducto[]>(`${this.urlEndPoint}/${fecha1}/${fecha2}`);
  }

  abrirModal():void{
    this.modal=true;
  }

  cerrarModal():void{
    this.modal=false;
  }

}
