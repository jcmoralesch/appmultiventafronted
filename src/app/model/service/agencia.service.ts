import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { Agencia } from '../class/agencia';
import { Observable, throwError } from 'rxjs';
import {map, catchError, switchAll} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AgenciaService {

  private urlEndPoint:string=URL_BACKEND+'/agencia';
  
  constructor(private http:HttpClient) { }

  public store(agencia:Agencia):Observable<Agencia>{
    return this.http.post<Agencia>(this.urlEndPoint,agencia,{headers:HTTPHEADERS}).pipe(
      map((response:any)=>response.agencia as Agencia),
      catchError(
        e=>{
          if(e.status==400){
            return throwError(e);
          }
          Swal.fire(e.error.mensaje,e.error.err,'error');
          return throwError(e);
        })
      )
  }

  public getAll():Observable<Agencia[]>{

    return this.http.get<Agencia[]>(this.urlEndPoint);
  }

  public getAllInCantidadProducto():Observable<Agencia[]>{
    return this.http.get<Agencia[]>(`${this.urlEndPoint}/cantidad-producto`);
  }
}
