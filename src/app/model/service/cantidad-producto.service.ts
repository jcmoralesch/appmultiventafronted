import { Injectable } from '@angular/core';
import { URL_BACKEND } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CantidadProducto } from '../class/cantidad-producto';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CantidadProductoService {

  private urlEndPoint:string=URL_BACKEND+'/cantidad-producto';

  constructor(private http:HttpClient) { }

  public getAll(agencia:string):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/${agencia}`).pipe(
      map(response=>{
        response.map(res=>{
           const nombreMayuscula =res.producto.nombre.toUpperCase();
          res.producto.nombre=nombreMayuscula;
          return res;
        })
        return response
      })
    );
  }

  public getByAgenciaAndCantidad(agencia:string):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/agencia/cant/${agencia}`);
  }

  public getByCantidad(agencia:string,cantidad:number):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/${agencia}/${cantidad}`);
  }

  public getByCantidadAll(cantidad:number):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/cantidad/${cantidad}`);
  }

  public getAllAgencias():Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}`).pipe(
      map(response=>{
        response.map(res=>{
           const nombreMayuscula =res.producto.nombre.toUpperCase();
          res.producto.nombre=nombreMayuscula;
          return res;
        })
        return response
      })
    );
  }

  public findProductosByFilter(marca:string,agencia:string,categoria:string):Observable<CantidadProducto[]>{
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/filtrar/${marca}/${agencia}/${categoria}`);
  }
}
