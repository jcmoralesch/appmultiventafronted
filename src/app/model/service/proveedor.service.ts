import { Injectable } from '@angular/core';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { Proveedor } from '../class/proveedor';
import { Observable, throwError } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  private urlEndPoint:string=URL_BACKEND+'/proveedor';
 

  constructor(private http:HttpClient) { }
  
  public store(proveedor:Proveedor):Observable<Proveedor>{
    return this.http.post(this.urlEndPoint,proveedor,{headers:HTTPHEADERS}).pipe(
       map((response:any)=>response.proveedor as Proveedor),
       catchError(e=>{
         if(e.status==400){
           return throwError(e);
         }
         Swal.fire(e.error.mensaje,e.error.err,'error');
         return throwError(e);
       })
    );
  }

  public getAll(page:number):Observable<any>{
    return this.http.get(`${this.urlEndPoint}/${page}`).pipe(
      map((response:any)=>{
        (response.content as Proveedor[]).map(
          proveedor=>{
            return proveedor;
          })
          return response;
      })
    )
  }

  public getAllPersonal():Observable<Proveedor[]>{
    return this.http.get<Proveedor[]>(`${this.urlEndPoint}`);
  }

  public getById(id:number):Observable<Proveedor>{
    return this.http.get<Proveedor>(`${this.urlEndPoint}/find/${id}`)
  }

  public update(proveedor:Proveedor):Observable<Proveedor>{
    return this.http.put<Proveedor>(`${this.urlEndPoint}/${proveedor.id}`,proveedor).pipe(
      map((response:any)=>response.content as Proveedor),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public delete(proveedor:Proveedor):Observable<Proveedor>{
    return this.http.put<Proveedor>(`${this.urlEndPoint}/update/${proveedor.id}`,proveedor);
  }
  
}
