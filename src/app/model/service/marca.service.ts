import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { Marca } from '../class/marca';
import { Observable, throwError } from 'rxjs';
import { catchError,map } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class MarcaService {

  private urlEndPoint:string=URL_BACKEND+'/marca';
  public modal:boolean=false;
  private _notificarCambio= new EventEmitter<any>();
  
  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  store(marca:Marca):Observable<Marca>{
    return this.http.post<Marca>(`${this.urlEndPoint}`,marca,{headers:HTTPHEADERS}).pipe(
      map((response:any)=>response.marca as Marca),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
    
  }

  public getAll():Observable<Marca[]>{
    return this.http.get<Marca[]>(`${this.urlEndPoint}`);
  }

  public abrirModal(){
    this.modal=true;
  }

  public cerrarModal(){
    this.modal=false;
  }

}
