import { Injectable, EventEmitter } from '@angular/core';
import { URL_BACKEND } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { Venta } from '../class/venta';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  public modal:boolean=false;
  private urlEndPoint:string=URL_BACKEND+'/venta';
  private _notificarCambio= new EventEmitter<any>();

  constructor(private http:HttpClient) { }

  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  public store(venta:Venta):Observable<Venta>{
    return this.http.post<Venta>(`${this.urlEndPoint}`,venta).pipe(
      map((response:any)=> response.venta as Venta),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
      
    )
  }

  public getByFechaAndAgencia(fecha1:string,fecha2:string,agencia:string):Observable<Venta[]>{
    return this.http.get<Venta[]>(`${this.urlEndPoint}/${fecha1}/${fecha2}/${agencia}`);
  }

  public getAllAgenciaAndDate():Observable<Venta[]>{
    return this.http.get<Venta[]>(`${this.urlEndPoint}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
