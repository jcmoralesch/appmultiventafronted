import { Injectable } from '@angular/core';
import { HttpClient,HttpRequest,HttpEvent} from '@angular/common/http';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import { Producto } from '../class/producto';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private urlEndPoint:string=URL_BACKEND+'/producto';
  public modal:boolean=false;

  constructor(private http:HttpClient) { }

  public store(producto:Producto):Observable<Producto>{
    return this.http.post<Producto>(`${this.urlEndPoint}`,producto,{headers:HTTPHEADERS}).pipe(
      map((response:any)=>response.producto as Producto),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public getAll():Observable<Producto[]>{
    return this.http.get<Producto[]>(`${this.urlEndPoint}`).pipe(
      map(response=>{
        response.map(res=>{
          const nombreMayuscula=res.nombre.toUpperCase();
          return res.nombre=nombreMayuscula;
        })
        return response;
      })
    );
  }

  public getById(id:number):Observable<Producto>{
    return this.http.get<Producto>(`${this.urlEndPoint}/${id}`);
  }

  public subirFoto(archivo:File,id):Observable<HttpEvent<{}>>{
    let formData = new FormData();
    formData.append("archivo",archivo);
    formData.append("id",id);

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`,formData, {
      reportProgress: true
    });

    return this.http.request(req);
  }

  public update(producto:Producto):Observable<Producto>{
    return this.http.put<Producto>(`${this.urlEndPoint}/${producto.id}`,producto).pipe(
      map((response:any)=>response.producto as Producto),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
