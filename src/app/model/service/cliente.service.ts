import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL_BACKEND } from 'src/app/config/config';
import { Cliente } from '../class/cliente';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  public modal:boolean=false;
  private urlEndpPoint:string=URL_BACKEND+'/cliente';
  private _notificarCambio= new EventEmitter<any>();
  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  public store(cliente:Cliente):Observable<Cliente>{
    return this.http.post<Cliente>(`${this.urlEndpPoint}`,cliente).pipe(
      map((response:any)=> response.cliente as Cliente),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        } 
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })

    )
  }

  public filtrarCliente(term:string):Observable<Cliente[]>{
    return this.http.get<Cliente[]>(`${this.urlEndpPoint}/filtrar-cliente/${term}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }

}
