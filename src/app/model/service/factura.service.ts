import { Injectable, EventEmitter } from '@angular/core';
import { URL_BACKEND } from 'src/app/config/config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Factura } from '../class/factura';
import { CantidadProducto } from '../class/cantidad-producto';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private urlEndPoint:string=URL_BACKEND+'/factura';
  private _notificarCambio= new EventEmitter<any>();
  public modal:boolean = false;
  public modalItem:boolean=false;
  
  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  filtrarProductos(term: string): Observable<CantidadProducto[]> {
    return this.http.get<CantidadProducto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  create(factura: Factura): Observable<Factura> {
    return this.http.post<Factura>(this.urlEndPoint, factura);
  }

 getAll():Observable<Factura[]>{
    return this.http.get<Factura[]>(`${this.urlEndPoint}`);
  }

  public getByCurrenDate():Observable<Factura[]>{
    return this.http.get<Factura[]>(this.urlEndPoint+'/current-date');
  }

  public finByFechaBetweenOrAgencia(fecha1:string, fecha2:string, agencia:string):Observable<Factura[]>{
    return this.http.get<Factura[]>(`${this.urlEndPoint}/get-by-date/${fecha1}/${fecha2}/${agencia}`);
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }

  public abrirModalItem():void{
    this.modalItem=true;
  }

  public cerrarModalItem():void{
    this.modalItem=false;
  }
  
}
