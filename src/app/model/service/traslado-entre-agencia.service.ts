import { Injectable, EventEmitter } from '@angular/core';
import { URL_BACKEND } from 'src/app/config/config';
import { HttpClient } from '@angular/common/http';
import { TrasladoEntreAgencia } from '../class/traslado-entre-agencia';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class TrasladoEntreAgenciaService {

  public modal:boolean=false;
  private urlEndPoint:string=URL_BACKEND+'/traslado-entre-agencia';
  private _notificarCambio= new EventEmitter<any>();
  
  get notificarCambio():EventEmitter<any>{
    return this._notificarCambio;
  }

  constructor(private http:HttpClient) { }

  public store(trasladoEntreAgencia:TrasladoEntreAgencia):Observable<TrasladoEntreAgencia>{
    return this.http.post<TrasladoEntreAgencia>(`${this.urlEndPoint}`,trasladoEntreAgencia).pipe(
      map((response:any)=>response.trasladoEntreAgencia as TrasladoEntreAgencia),
      catchError(e=>{
        if(e.status==400){
          return throwError(e);
        }
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

  public abrirModal():void{
    this.modal=true;
  }

  public cerrarModal():void{
    this.modal=false;
  }
}
