import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Personal } from '../class/personal';
import { Observable, throwError } from 'rxjs';
import { URL_BACKEND, HTTPHEADERS } from 'src/app/config/config';
import {map, catchError} from 'rxjs/operators';
import Swal from 'sweetalert2';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class PersonalService {
  
  private urlEndPoint:string=URL_BACKEND+'/personal';

  constructor(private http:HttpClient) { }

  public store(personal:Personal):Observable<Personal>{
    return this.http.post(this.urlEndPoint,personal,{headers:HTTPHEADERS}).pipe(
         map((response:any)=>response.personal as Personal),
         catchError(e=>{
           if(e.status==400){
             return throwError(e)
           }
           Swal.fire(e.error.mensaje,e.error.err,'error');
           return throwError(e);
         })
    )
  }

  public getAll(page:number):Observable<any>{
    return this.http.get(`${this.urlEndPoint}/${page}`).pipe(
      map((response:any)=>{
       (response.content as Personal[]).map(
         personal=>{
           return personal;
         });
        return response;
      })
    );
  }

  public getById(id:number):Observable<Personal>{
    return this.http.get<Personal>(`${this.urlEndPoint}/personal/${id}`);
  }

  public update(personal:Personal):Observable<Personal>{
    return this.http.put<Personal>(`${this.urlEndPoint}/update/${personal.id}`,personal).pipe(
      map((response:any)=>response.content as Personal),
      catchError(
        e=>{
          if(e.status==400){
            return throwError(e);
          }
          Swal.fire(e.error.mensaje,e.error.err,'error');
          return throwError(e);
        }
      )
    )
  }

  public delete(id:number):Observable<Personal>{
    return this.http.delete<Personal>(`${this.urlEndPoint}/delete/${id}`).pipe(
      catchError(e=>{
        Swal.fire(e.error.mensaje,e.error.err,'error');
        return throwError(e);
      })
    )
  }

}
