import { Categoria } from './categoria';
import { Marca } from './marca';
import { Proveedor } from './proveedor';

export class Producto {
    public id:number;
    public precio:number=0.00;
    public nombre:string='';
    public categoria:Categoria=null;
    public codigo:string='';
    public precioDocena:number = 0.00;
    public precioMayor:number = 0.00;
    public precioVenta:number=0.00;
    public alerta:number=0;
    public marca:Marca=null;
    public proveedor:Proveedor=null;
    
}
