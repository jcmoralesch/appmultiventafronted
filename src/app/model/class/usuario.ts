import { Role } from './role';
import { Personal } from './personal';

export class Usuario {
  public username:string;
  public password:string;
  public role:Role[];
  public id:number;
  public personal:Personal;
}
