import { Agencia } from './agencia';

export class Personal {
    id:number=0;
    identificacion:string='';
    nombre:string='';
    apellido:string='';
    telefono:string='';
    direccion:string='';
    agencia:Agencia;

}
