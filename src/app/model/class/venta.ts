import { Producto } from './producto';

export class Venta {

    public precioVenta:number=0.00;
    public producto:Producto;
    public cantidad:number;
    public ganancia:number;
}
