import { Agencia } from './agencia';
import { CantidadProducto } from './cantidad-producto';

export class TrasladoEntreAgencia {

    public cantidad:number=0;
    public fechaRegistro:string='';
    public agenciaOrigen:Agencia=null;
    public agenciaDestino:Agencia=null;
    public cantidadProducto:CantidadProducto=null;
    
}
