import { Producto } from './producto';
import { Agencia } from './agencia';

export class CantidadProducto {
    public id:number;
    public producto:Producto;
    public cantidad:number;
    public agencia:Agencia;
}
