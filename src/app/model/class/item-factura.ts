
import { CantidadProducto } from './cantidad-producto';

export class ItemFactura {
    producto:CantidadProducto;
    cantidad:number=1;
    importe:number;
    precioVenta:number=0;
    numeroFacutura:number;

    public calcularImporte():number{
        return this.cantidad * this.producto.producto.precioVenta;
    }
}
