import { ItemFactura } from './item-factura';
import { Cliente } from './cliente';
import { Usuario } from './usuario';

export class Factura {

    id:number;
    items:Array<ItemFactura> =[];
    total:number;
    fechaRegistro:string;
    cliente:Cliente;
    horaRegistro:string;
    usuario:Usuario;

    calcularGranTotal():number{
        this.total=0;
        this.items.forEach((item: ItemFactura)=>{
            this.total += item.calcularImporte();
        });
        return this.total;
    }
}
