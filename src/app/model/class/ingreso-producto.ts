import { Producto } from './producto';

export class IngresoProducto {

    public producto:Producto;
    public cantidad:number;
    public traslado:number;
    public total:number;
}
