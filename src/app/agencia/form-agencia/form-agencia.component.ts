import { Component, OnInit } from '@angular/core';
import { Agencia } from 'src/app/model/class/agencia';
import { AgenciaService } from 'src/app/model/service/agencia.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-agencia',
  templateUrl: './form-agencia.component.html',
  styleUrls: ['./form-agencia.component.css']
})
export class FormAgenciaComponent implements OnInit {

  public agencia:Agencia = new Agencia();

  constructor(private agenciaService:AgenciaService,
              private router:Router) { }

  ngOnInit() {
  }

  store():void{
    this.agenciaService.store(this.agencia).subscribe(
      agencia=>{
         this.router.navigate(['/agencia']);
         Swal.fire('Registro nuevo ',`Agencia ${agencia.nombre} registrada con éxito`,'success');
      }
    )
  }

}
