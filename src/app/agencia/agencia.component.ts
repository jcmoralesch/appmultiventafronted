import { Component, OnInit } from '@angular/core';
import { AgenciaService } from '../model/service/agencia.service';
import { Agencia } from '../model/class/agencia';

@Component({
  selector: 'app-agencia',
  templateUrl: './agencia.component.html',
  styleUrls: ['./agencia.component.css']
})
export class AgenciaComponent implements OnInit {
  
  public agencias:Agencia[];

  

  constructor(private agenciaService:AgenciaService) { }

  ngOnInit() {
    this.getAll();
   
  }

  getAll():void{
    this.agenciaService.getAll().subscribe(
      agencias=>this.agencias=agencias
    )
  }

}
