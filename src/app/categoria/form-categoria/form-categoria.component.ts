import { Component, OnInit, Input } from '@angular/core';
import { CategoriaService } from 'src/app/model/service/categoria.service';
import { ToastrService } from 'ngx-toastr';
import { Categoria } from 'src/app/model/class/categoria';

@Component({
  selector: 'app-form-categoria',
  templateUrl: './form-categoria.component.html',
  styleUrls: ['./form-categoria.component.css']
})
export class FormCategoriaComponent implements OnInit {

  @Input() categoria:Categoria;

  constructor(public categoriaService:CategoriaService,
              private toastr:ToastrService) { }

  ngOnInit() {
  }

  public store():void{
    this.categoriaService.store(this.categoria).subscribe(
      categoria=>{
        this.categoriaService.notificarCambio.emit();//para actualizar datos en la tabla
        this.cerrarModal();
        this.toastr.success('Operación realizada con éxito','Success');
      },
      err=>{

      }
    )
  }

  public cerrarModal(){
    this.categoriaService.cerrarModal();
  }


}
